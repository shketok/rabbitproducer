/*
 * Copyright 2023 Sreda Software Solutions. All rights reserved.
 * The copyright notice above does not evidence any actual or
 * intended publication of such source code. The code contains
 * Sreda Software Solutions Confidential Proprietary Information.
 */

package com.example.rabbit.controller;


import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class RabbitController {

    private final RabbitTemplate rabbitTemplate;

    @PostMapping("/post")
    public void post() {
        rabbitTemplate.convertAndSend(
                "exchange.events.sd.cam",
                "events.sd.cam",
                "Hi!"
        );
    }
}
